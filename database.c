/* Database of the Police Department in Los Santos */
/* last database update: June 26, 2022 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>



int main() {
  printf("\nWelcome to the LSPD database\n");

  char verfname[20] = "0";
  char verlname[20] = "0";
  char verplate[8] = "0";
  int pv = 0;

  printf("\n");
  printf("Press (1) to check an ID...\n");
  printf("Press (2) to check a License Plate...\n");
  printf("Press (0) to exit the LSPD database...\n");

  /* choosing the identification */
  scanf("%d", &pv);
  printf("\n");




  /* Person Check */
  if(pv == 1) {
    printf("Personal Information for: ");
    scanf("%s %s", verfname, verlname);
    printf("\nSearching information about [%s %s]...\n\n", verfname, verlname);
    printf("- - - Info - - -\n");
    printf("First Name:                         %s\n", verfname);
    printf("Last Name:                          %s\n", verlname);
    printf("\n");

    /* RANDOMIZING PERSONAL INFORMATION */
    int i, n;
    time_t t;
    int license = 0;
    int wanted_person = 0;
    int guns = 0;
    int fishing = 0;
    int flags_person = 0;

    n = 5;

    /*Initializes random number generator */
    srand((unsigned) time(&t));

    /* Print n random numbers from |modulo n| cases */
    for(i = 0; i < n; i++) {
      license = ( rand() % 4 );
      wanted_person = ( rand() % 4 );
      guns = ( rand() % 3 );
      fishing = ( rand() % 3 );
      flags_person = ( rand() % 25 );
    }
    /* LICENSE */
    printf("License:                            ");
    switch (license) {
      case 0: printf("Expired\n");          break;
      case 1: printf("Valid\n");            break;
      case 2: printf("Valid\n");            break;
      case 3: printf("Valid\n");            break;
    }

    /* WANTED STATUS */
    printf("Wanted Status:                      ");
    switch (wanted_person) {
      case 0: printf("Wanted\n");                break;
      case 1: printf("Not Wanted\n");            break;
      case 2: printf("Not Wanted\n");            break;
      case 3: printf("Not Wanted\n");            break;
    }

    /* GUN PERMISSION */
    printf("Gun Permission:                     ");
    switch (guns) {
      case 0: printf("NO\n");             break;
      case 1: printf("NO\n");             break;
      case 2: printf("YES\n");            break;
    }

    /* FISHING LICENSE */
    printf("Fishing License:                    ");
    switch (fishing) {
	case 0: printf("NO\n");			break;
	case 1: printf("NO\n");			break;
	case 2: printf("YES\n");		break;
    }

    /* FLAGS */
    printf("Flags:                              ");
    switch (flags_person) {
      case 0: printf("Known Child Predator\n");                 break;
      case 1: printf("Drunk in Public\n");                      break;
      case 2: printf("Resisting Arrest\n");                     break;
      case 3: printf("Warrant for Arrest\n");                   break;
      case 4: printf("Present during Riot\n");                  break;
      case 5: printf("Impersonating a Peace Officer\n");        break;
      case 6: printf("Participation in Gang During Felony\n");  break;
      case 7: printf("Harrasment\n");                           break;
      case 8: printf("Abuse of Drugs\n");                       break;
      case 9: printf("None\n");                                 break;
      case 10: printf("None\n");                                break;
      case 11: printf("None\n");                                break;
      case 12: printf("None\n");                                break;
      case 13: printf("None\n");                                break;
      case 14: printf("None\n");                                break;
      case 15: printf("None\n");                                break;
      case 16: printf("None\n");                                break;
      case 17: printf("None\n");                                break;
      case 18: printf("None\n");                                break;
      case 19: printf("None\n");                                break;
      case 20: printf("None\n");                                break;
      case 21: printf("None\n");                                break;
      case 22: printf("None\n");                                break;
      case 23: printf("None\n");                                break;
      case 24: printf("None\n");                                break;
    }



    printf("\n");
  }







  /* Vehicle Check */
  else if(pv == 2) {
    printf("Plate #: ");
    scanf("%s", verplate);
    printf("\nPerforming Vehicle check for [%s]...\n\n", verplate);
    printf("- - - Info - - -\n");

    /* RANDOMIZING VEHICLE INFORMATION */
    int i, n;
    time_t t;
    int registration = 0;
    int insurance = 0;
    int inspection = 0;
    int stolen = 0;
    int wanted = 0;
    int flags = 0;

    n = 6;

    /*Initializes random number generator */
    srand((unsigned) time(&t));

    /* Print n random numbers from |modulo n| cases */
    for(i = 0; i < n; i++) {
      registration = ( rand() % 5 );
      insurance = ( rand() % 4 );
      inspection = ( rand() % 5 );
      stolen = ( rand() % 4 );
      wanted = ( rand() % 4 );
      flags = ( rand() % 20 );
    }


    /* REGISTRATION STATUS */
    printf("Registration Status:                    ");
    switch (registration) {
      case 0: printf("Expired\n");          break;
      case 1: printf("Suspended\n");	    break;
      case 2: printf("Valid\n");            break;
      case 3: printf("Valid\n");            break;
      case 4: printf("Valid\n");            break;
    }




    /* INSURANCE STATUS */
    printf("Insurance Status:                       ");
    switch (insurance) {
      case 0: printf("Expired\n");          break;
      case 1: printf("Valid\n");            break;
      case 2: printf("Valid\n");            break;
      case 3: printf("Valid\n");            break;
    }




    /* INSPECTION STATUS */
    printf("Inspection Status:                      ");
    switch (inspection) {
      case 0: printf("Expired\n");          break;
      case 1: printf("Failed Inspection\n");break;
      case 2: printf("Valid\n");            break;
      case 3: printf("Valid\n");            break;
      case 4: printf("Valid\n");            break;
    }



    /* STOLEN STATUS */
    printf("Stolen Status:                          ");
    switch (stolen) {
      case 0: printf("Stolen\n");                 break;
      case 1: printf("Not Stolen\n");             break;
      case 2: printf("Not Stolen\n");             break;
      case 3: printf("Not Stolen\n");             break;
    }


    /* WANTED STATUS */
    printf("Wanted Status:                          ");
    switch (wanted) {
      case 0: printf("Wanted\n");                 break;
      case 1: printf("Not Wanted\n");             break;
      case 2: printf("Not Wanted\n");             break;
      case 3: printf("Not Wanted\n");             break;
    }



    /* FLAGS */
    printf("Flags:                                  ");
    switch (flags) {
      case 0: printf("Illegal Tint on Windows\n");                 break;
      case 1: printf("Staying on the handicapped parking \n");                      break;
      case 2: printf("Driving with a broken windshield\n");                     break;
      case 3: printf("Driving 1 month without insurance\n");                   break;
      case 4: printf("Driving 1 week without registration\n");                  break;
      case 5: printf("Multiple failed inspections\n");        break;
      case 6: printf("Broken indicators\n");  break;
      case 7: printf("Vehicle height not satisfying Californian Laws\n");                           break;
      case 8: printf("Loud exhaust\n");                       break;
      case 9: printf("None\n");                                 break;
      case 10: printf("None\n");                                break;
      case 11: printf("None\n");                                break;
      case 12: printf("None\n");                                break;
      case 13: printf("None\n");                                break;
      case 14: printf("None\n");                                break;
      case 15: printf("None\n");                                break;
      case 16: printf("None\n");                                break;
      case 17: printf("None\n");                                break;
      case 18: printf("None\n");                                break;
      case 19: printf("None\n");                                break;
      case 20: printf("None\n");                                break;
      case 21: printf("None\n");                                break;
      case 22: printf("None\n");                                break;
      case 23: printf("None\n");                                break;
      case 24: printf("None\n");                                break;
    }

  }

  /* Logging out */
  else if(pv == 0) {
    printf("Logging out...\n\n");
  }
  /* ERROR */
  else {
    printf("[404] ERROR: Selected option invalid\n\n");
  }

}
