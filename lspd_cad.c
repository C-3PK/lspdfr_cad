/* LSPD official Computer Aided Dispatch created by the IT Police Department in Los Santos */
/* version 1.0 created on July 5, 2021, 11:50:08 */
/* last edited on June 26, 2022, 22:38:23 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main() {

  /* login confirmation */
  int id = 0;
  int value;
  char passwrd[20];
  int loggedin = 0;
  int loggedInId = 0;

  /* Officer John Malowski */
  int id_mal = 189;
  char rank_mal[] = "Officer";
  char firstname_mal[] = "John";
  char lastname_mal[] = "Malowski";
  char birth_mal[] = "06/08/1985";
  char passwrd_mal[] = "iLoveMyCVPI";

  /* Officer Jeff Favignano */
  int id_fav = 127;
  char rank_fav[] = "Officer";
  char firstname_fav[] = "Jeff";
  char lastname_fav[] = "Favignano";
  char birth_fav[] = "07/11/1983";
  char passwrd_fav[] = "fivePDeverybody!";

  printf("LSPD, please log in to access the software...\n");
  printf("ID: ");
  scanf("%d", &id);
  printf("Password: ");
  scanf("%s", passwrd); 
  printf("\n");
  printf("Logging in...\n");
  printf("\n"); 


  if (id == id_mal) {
    value=strcmp(passwrd,passwrd_mal);  
    if(value!=0) {
      printf("\nWrong password, please try again.\n\n");
    }
    
    else {
      printf("-----------------------------------------------------------------------------------------------------------------------\n");
      printf("  -------------------------------------\n");
      printf("      Welcome back, %s %s!\n", rank_mal, lastname_mal);
      printf("  -------------------------------------\n\n");
      loggedInId = 189;
      
      /* Choosing the Patrol Station */
      int i, n;
      time_t t;
      int patrol = 0;

      n = 1;

      /*Initializes random number generator */
      srand((unsigned) time(&t));

      /* Print 1 random number from 0 to 7 */
      for(i = 0; i < 1; i++) {
        patrol = ( rand() % 10 );
      }

      /* Duty information and status */

      printf("Los Santos Police Department: %s", ctime(&t));
      printf("Identification Number:        * %d *\n", loggedInId);
      printf("Currently logged in as:       %s %s %s\n", rank_mal, firstname_mal, lastname_mal);
      printf("Today's Patrol:               ");
      /* LSPD Stations */
      switch (patrol) {
        case 0: printf("Davis Sheriff's Station\n");        break;
        case 1: printf("Paleto Bay Sheriff's Station\n");   break;
        case 2: printf("Sandy Shores Sheriff's Station\n"); break;
        case 3: printf("La Mesa Police Station\n");         break;
        case 4: printf("Mission Row Police Station\n");     break;
        case 5: printf("Rockford Hills Police Station\n");  break;
        case 6: printf("Vespucci Police Station\n");        break;
        case 7: printf("Vespucci Beach Police Station\n");  break;
        case 8: printf("Vinewood Police Station\n");        break;
        case 9: printf("Bolingbroke Penitentiary\n");       break;
        case 10: printf("Del Perro Police Station\n");      break;
      }
      printf("\n");
      printf("-----------------------------------------------------------------------------------------------------------------------\n");
    }
    
    
  }
  else if (id == id_fav) {
    value=strcmp(passwrd,passwrd_fav);  
    if(value!=0) {
      printf("\nWrong password, please try again.\n\n");
    }
    
    else {
      printf("-----------------------------------------------------------------------------------------------------------------------\n");
      printf("  -------------------------------------\n");
      printf("      Welcome back, %s %s!\n", rank_fav, lastname_fav);
      printf("  -------------------------------------\n\n");
      loggedInId = 127;
      
      /* Choosing the Patrol Station */
      int i, n;
      time_t t;
      int patrol = 0;

      n = 1;

      /*Initializes random number generator */
      srand((unsigned) time(&t));

      /* Print 1 random number from 0 to 7 */
      for(i = 0; i < 1; i++) {
        patrol = ( rand() % 10 );
      }

      /* Duty information and status */

      printf("Los Santos Police Department: %s", ctime(&t));
      printf("Identification Number:        * %d *\n", loggedInId);
      printf("Currently logged in as:       %s %s %s\n", rank_fav, firstname_fav, lastname_fav);
      printf("Today's Patrol:               ");
      /* LSPD Stations */
      switch (patrol) {
        case 0: printf("Davis Sheriff's Station\n");        break;
        case 1: printf("Paleto Bay Sheriff's Station\n");   break;
        case 2: printf("Sandy Shores Sheriff's Station\n"); break;
        case 3: printf("La Mesa Police Station\n");         break;
        case 4: printf("Mission Row Police Station\n");     break;
        case 5: printf("Rockford Hills Police Station\n");  break;
        case 6: printf("Vespucci Police Station\n");        break;
        case 7: printf("Vespucci Beach Police Station\n");  break;
        case 8: printf("Vinewood Police Station\n");        break;
        case 9: printf("Bolingbroke Penitentiary\n");       break;
        case 10: printf("Del Perro Police Station\n");      break;
      }
      printf("\n");
      printf("-----------------------------------------------------------------------------------------------------------------------\n");
    }
  }
  
  else {
    printf("\nSomething went wrong, please try again.\n\n");
    return 0;
  }


 return 0;
}
